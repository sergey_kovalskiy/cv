# Resume for Sergey

Taken from Jürno Ader's resume, who did most of the work setting this up as proper repo.

1. Clone the repository
2. `npm install`
3. `npm run serve`
4. Update `resume.json` file and see result in browser

- **Generate PDF:** `npm run export:pdf`
- **Generate HTML:** `npm run export:html`
